# coding=utf8

# TODO: uncomment this (needed for the real colour class)
# from neopixel import *

STATION_TO_LED_MAP = {
    'EGNX': 1,
    'EGCN': 2,
    'KPIT': 3,
    'KPGA': 4,
    'KPSP': 5
}

BADGE_CLASSES = {
    'VFR': 'badge-success',
    'MVFR': 'badge-warning',
    'IFR': 'badge-danger',
    'LIFR': 'badge-dark'
}


# TODO: dunno what the Colour class is, presumably RGB
# TODO: remove this dummy class
class Color(object):
    def __init__(self, red, green, blue):
        self.r = red
        self.g = green
        self.b = blue

    def __str__(self):
        return 'Colour {r}, {g}, {b}'.format(r=self.r, g=self.g, b=self.b)


LED_COLOURS = {
    'VFR': Color(0, 255, 0),
    'MVFR': Color(0, 0, 255),
    'IFR': Color(255, 0, 0),
    'LIFR': Color(125, 0, 125)
}

QUERY_URL = 'https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString={stations}&hoursBeforeNow=24&mostRecentForEachStation=true'
