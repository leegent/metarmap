# METAR Map
## Dumb METAR grabber and LED lighter

To install:
```
pip install -r requirements.txt
```

To run:
```
export FLASK_APP=metarmap.py
flask run --host=0.0.0.0
```

Now you can browse to your machine, port 5000.

Install your LED library thing then follow the TODOs to enable LED output.
