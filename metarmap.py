# coding=utf-8

# TODO: uncomment this
#from neopixel import *

import datetime
import requests
from flask import Flask, render_template
from xml.etree import ElementTree

from data import STATION_TO_LED_MAP, BADGE_CLASSES, QUERY_URL, LED_COLOURS

app = Flask(__name__)
strip = None


class Station(object):
    def __init__(self, name, led_id, conditions):
        self.name = name
        self.led_id = led_id
        self.conditions = conditions
        self.badge_class = BADGE_CLASSES[self.conditions]
        self.led_colour = LED_COLOURS[self.conditions]


def light_led(led_id, colour):
    # TODO: uncomment these lines and remove the print once the real LED libraries are around
    # strip.setPixelColor(led_id, colour)
    # strip.show()
    print('Lighting LED {led_id} to {colour}'.format(led_id=led_id, colour=colour))


@app.route('/')
def hello_world():
    now = datetime.datetime.now()
    utcnow = datetime.datetime.utcnow()

    # For each station LED, retrieve METAR and generate LED colours

    # 1 Construct query
    stations = STATION_TO_LED_MAP.keys()
    stations = '%20'.join(stations)
    query_url = QUERY_URL.format(stations=stations)

    # 2 Execute query
    response = requests.get(query_url)

    # 3 Parse response into list of station data
    stations = []
    station_to_conditons = dict()
    xmltree = ElementTree.fromstring(response.content)
    for station in xmltree.iter('METAR'):
        station_name = station.find('station_id').text
        flight_conditions = station.find('flight_category').text
        station_to_conditons[station_name] = flight_conditions

        stations.append(Station(station_name, STATION_TO_LED_MAP[station_name], flight_conditions))

    # 4 Light the LEDs
    for station in stations:
        light_led(station.led_id, station.led_colour)

    # 5 Render a nice HTML page

    return render_template('index.html', stations=stations, updated=now.ctime(), updatedutc=utcnow.ctime())


def init_strip():
    # LED strip configuration:
    LED_COUNT = 50  # Number of LED pixels.
    LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
    # LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
    LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA = 5  # DMA channel to use for generating signal (try 5)
    LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
    LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
    LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53
    LED_STRIP = ws.WS2811_STRIP_GRB  # Strip type and colour ordering

    _strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL,
                              LED_STRIP)

    _strip.begin()
    return _strip


if __name__ == '__main__':
    # TODO: UNCOMMENT ME vvvvv
    # strip = init_strip()
    app.run()
